#!/usr/bin/env python
# coding: utf-8
    
# In[2]:

### CE SCRIPT À POUR BUT DE RECUPÉRER UN FICHIER .LOG ET D'EN FAIRE UN FICHIER .CSV
    
###############################################################################################
###############################################################################################

import csv
import re

with open('small-data.log', 'r') as file:
	with open('small_data.csv', 'w', newline='') as csvfile:
		fieldnames =['ip_adress','date','requete_demande','http_status','donnee_inconnu','donnees_complementaire','origine_demande','fin']
		writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
		writer.writeheader()
		for line in file:
			matching = re.match(r"(\"*(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}).*\[(.*)\]\s\"*([^\"]*)\"*([^\"]*)\"*([^\"]*)\"*\s*\"*([^\";]*)([^\"]*))>",line)
			ip_adress,date,requet_demande,http_status,donnee_inconnu,donnees_complementaire,origine_demande,fin = matching.groups()
			writer.writerow({'ip_adress':ip_adress, 'date':date,'requete_demande':requet_demande,'http_status':http_status,'donnee_inconnu':donnee_inconnu,'donnees_complementaire':donnees_complementaire,'origine_demande':origine_demande,'fin':fin})
