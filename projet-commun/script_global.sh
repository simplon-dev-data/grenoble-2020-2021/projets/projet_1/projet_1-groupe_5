#!/bin/bash

# Ce script automatise le traitement de données, d'un fichier .log trop volumineux pour être traiter seul
#
# Utilisation:
#   ./script_global.sh


##############################################################################
# Partie 1 : extraction de données
##############################################################################

echo  "Transformation du dataset de .log à .csv "
python3 script.py

##############################################################################
# Partie 2 : chargement de données
##############################################################################

echo "Chargement des données en base ${DATABASE_FILE} (load.sql)"
sqlite3 datatest.db ".read load.sql"
